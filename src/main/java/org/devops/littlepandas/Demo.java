package org.devops.littlepandas;

import org.devops.littlepandas.lib.Aggregate;
import org.devops.littlepandas.lib.Dataframe;
import org.devops.littlepandas.lib.Statistics;

public class Demo {
    public static void main(String[] args) throws Exception {

        Dataframe df = new Dataframe(args[0]);
        System.out.println("############# Original #############");
        System.out.println(df.printAll());

        System.out.println("############ First lines ###########");
        System.out.println(df.printFirstLines());
        System.out.println("############ Last lines  ###########");
        System.out.println(df.printLastLines());

        Object[][][] groupedby = Statistics.groupBy(df.getData(), "Ville", false);

        System.out.println("######### Grouped By Ville #########");
        Aggregate a = new Aggregate(groupedby, "Ville");
        System.out.println(a.combine().printAll());

        System.out.println("### Select Where Ville=Grenoble ####");
        Dataframe grenobleData= a.get(0);
        System.out.println(grenobleData.printAll());

        System.out.println("########### Sort by Nom ############");
        Dataframe sortByNom= a.get(0).sort("Nom");
        System.out.println(sortByNom.printAll());

        System.out.println("##### Select Where Taille=1.65 ######");
        Dataframe SelectTaille= a.get(0).sort("Nom").search("Taille",1.65);
        System.out.println(SelectTaille.printAll());
        
        System.out.println("##### Min, max, mean with loc on Age #####");
        System.out.println("## Min : ");
        System.out.println(SelectTaille.loc("Age").min().printAll());
        System.out.println("## Max : ");
        System.out.println(SelectTaille.loc("Age").max().printAll());
        System.out.println("## Mean : ");
        System.out.println(SelectTaille.loc("Age").mean().printAll());
    }
}
