package org.devops.littlepandas.lib;
/**
 * @author Mickael KARCHE
 */

import java.util.ArrayList;

/**
 * This class is to apply Statistic function on multiple group of data
 */
public class Aggregate {
    Dataframe[] groupedBy;
    String groupedOn;

    /**
     * @param groupedBy create an Array of multiple Dataframe
     * @throws Exception 
     */
    public Aggregate(Object[][][] groupedBy, String groupedOn) throws Exception {
        this.groupedBy = new Dataframe[groupedBy.length];
        for (int i = 0; i < groupedBy.length; i++) {
            this.groupedBy[i] = new Dataframe(groupedBy[i]);
        }
        this.groupedOn = groupedOn;
    }

    /**
     * @param axis
     * @return a new aggregate with multiple dataframe which are the results of the sum for each group
     * @throws Exception 
     */
    public Aggregate sum(int axis) throws Exception {
        Object[][][] out = new Object[groupedBy.length][][];
        Object[] tmp;
        for (int i = 0; i < groupedBy.length; i++) {
            if (axis == 0) {
                out[i] = new Object[2][];
                out[i][0] = groupedBy[0].getData()[0];
                out[i][1] = Statistics.sum(groupedBy[i].getData(), axis, groupedOn);
            } else {
                tmp = Statistics.sum(groupedBy[i].getData(), axis, groupedOn);
                out[i] = new Object[tmp.length + 1][];
                out[i][0] = new Object[]{"sum"};
                for (int j = 0; j < tmp.length; j++) {
                    out[i][j + 1] = new Object[]{tmp[j]};
                }
            }
        }
        return new Aggregate(out,groupedOn);
    }

    /**
     * @return a new aggregate with multiple dataframe which are the results of the mean for each group
     * @throws Exception 
     */
    public Aggregate mean() throws Exception {
        Object[][][] out = new Object[groupedBy.length][2][];
        for (int i = 0; i < groupedBy.length; i++) {
            out[i][0] = groupedBy[0].getData()[0];
            Object[] tmp= Statistics.mean(groupedBy[i].getData());
            for (int j = 0; j < tmp.length; j++) {
                if(groupedBy[i].getData()[0][j].equals(groupedOn)){
                    tmp[j]=groupedBy[i].getData()[1][j];
                }
            }
            out[i][1] = tmp;
        }
        return new Aggregate(out,groupedOn);
    }

    /**
     * @return a new aggregate with multiple dataframe which are the results of the min for each group
     * @throws Exception 
     */
    public Aggregate min() throws Exception {
        Object[][][] out = new Object[groupedBy.length][2][];
        for (int i = 0; i < groupedBy.length; i++) {
            out[i][0] = groupedBy[0].getData()[0];
            Object[] tmp= Statistics.min(groupedBy[i].getData());
            for (int j = 0; j < tmp.length; j++) {
                if(groupedBy[i].getData()[0][j].equals(groupedOn)){
                    tmp[j]=groupedBy[i].getData()[1][j];
                }
            }
            out[i][1] = tmp;
        }
        return new Aggregate(out,groupedOn);
    }

    /**
     * @return a new aggregate with multiple dataframe which are the results of the max for each group
     * @throws Exception 
     */
    public Aggregate max() throws Exception {
        Object[][][] out = new Object[groupedBy.length][2][];
        for (int i = 0; i < groupedBy.length; i++) {
            out[i][0] = groupedBy[0].getData()[0];
            Object[] tmp= Statistics.max(groupedBy[i].getData());
            for (int j = 0; j < tmp.length; j++) {
                if(groupedBy[i].getData()[0][j].equals(groupedOn)){
                    tmp[j]=groupedBy[i].getData()[1][j];
                }
            }
            out[i][1] = tmp;
        }
        return new Aggregate(out,groupedOn);
    }


    /**
     * @return a string which print all dataframe included in this Aggregate
     */
    public String printAll() {
        StringBuilder out = new StringBuilder();
        for (Dataframe dataframe : groupedBy) {
            out.append(dataframe.printAll()).append("\n");
        }
        return out.toString();
    }

    /**
     * @return a combined array of all grouped array
     * @throws Exception 
     */
    public Dataframe combine() throws Exception {
        Object[][] out;
        ArrayList<Object[]> list = new ArrayList<>();

        for (Dataframe dataframe : groupedBy) {
            for (int j = 1; j < dataframe.getData().length; j++) {
                list.add(dataframe.getData()[j]);
            }
        }
        out = new Object[list.size() + 1][];
        out[0] = groupedBy[0].getData()[0];
        for (int i = 0; i < list.size(); i++) {
            out[i + 1] = list.get(i);
        }
        return new Dataframe(out);
    }

    /**
     * @param i
     * @return the i-th dataframe from this Aggregate
     */
    public Dataframe get(int i) {
        return groupedBy[i];
    }


}
