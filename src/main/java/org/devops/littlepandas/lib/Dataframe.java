package org.devops.littlepandas.lib;

import java.util.ArrayList;

public class Dataframe {
	Object[][] data;
	Boolean first;//true if it's the first line
	public Dataframe(Object [][] data) throws Exception{
		this.data=data;
		
		if (data.length > 1) {
			for (int i = 0; i < data[0].length; i++) {
				if (!this.wellTyped(i)) throw new Exception("Missing value");
			}
		}
		first=true;
	}

	public Object[][] getData() {
		return data;
	}

	public Dataframe(String fileName) throws Exception{
		Parsing parsing=new Parsing(fileName);
		this.data=parsing.createArrayType();
		
		if (data.length > 1) {
			for (int i = 0; i < data[0].length; i++) {
				if (!this.wellTyped(i)) throw new Exception("Missing value");
			}
		}
		first=true;
	}
	//returns the size of the largest word for each column (used for display)
	private int[] getMaxSize(){
		int[] max=new int [data[0].length];
		for (int i = 0; i < max.length-1; i++) {
			max[i]=0;
		}
		for (Object[] datum : data) {
			for (int j = 0; j < datum.length; j++) {
				if (max[j] < datum[j].toString().length()) {
					max[j] = datum[j].toString().length();
				}
			}
		}
		return max;
	}

	//displays the dataset column from begin to end
	private String toString (int begin, int end){
		StringBuilder res = new StringBuilder();
		int [] lengthTab=getMaxSize();
		if(first){//for add labels
			res.append(" ".repeat(Integer.toString(end).length()+1));
			for (int i = 0; i < data[0].length; i++) {
				int lengthCase=data[0][i].toString().length();
				res.append(" ".repeat(lengthTab[i] - lengthCase)).append(data[0][i]).append("|"); //column name
			}
			res.append("\n");
		}
		if(begin==0)
			begin=1;
		for (int i = begin; i <= end; i++) {
			res.append(i).append(" ".repeat(Integer.toString(end).length()-Integer.toString(i).length()+1));
			for (int j = 0; j < data[i].length ; j++) {
				int lengthCase=data[i][j].toString().length();
				res.append(" ".repeat(lengthTab[j] - lengthCase)).append(data[i][j]).append("|");
			}
			res.append("\n");
		}
		return res.toString();
	}


	/**
	 *
	 * @return string representation of the all dataframe
	 */
	public String printAll(){
		if (data.length==0){
			return " ";
		}else if(data.length==1){//only labels
			StringBuilder res= new StringBuilder();
			for (int i = 0; i < data[0].length; i++) {
				res.append(data[0][i]).append("| "); //column name
			}
			return res.toString();
		}
		return toString(0, data.length-1);
	}

	/**
	 *
	 * @return the five first lines of the dataframe
	 */
	public String printFirstLines() {
		if (data.length <= 6)
			return printAll();

		return(toString(0, 5));
	}
	/**
	 *
	 * @return the last five lines of the dataframe
	 */
	public String printLastLines(){
		if (data.length <= 6)
			return printAll();

		return(toString(data.length-5,data.length-1));
	}

	/**
	 * @param  index
	 * @return returns the rows according to the indices contained in index
	 */
	public Dataframe iloc(int[] index) throws Exception {
		Object[][] out= new Object[index.length+1][];
		out[0]=data[0];
		for (int i = 0; i < index.length; i++) {
			if (index[i] <= data.length && index[i] > 0) {
			out[i+1]=data[index[i]];
			} else {
				throw new IllegalArgumentException("be careful, the index is negative or too big");
			}
		}

		return new Dataframe(out);

	}
	/**
	 * @param  index
	 * @return returns the rows according to the boolean contained in index
	 */

	public Dataframe iloc(Boolean[] index) throws Exception {
		Object[][] out;
		ArrayList<Object[]> arrayList=new ArrayList<>();

		if(index.length!=data.length-1){
			throw new IllegalArgumentException("boolean array is not full");
		}
		for (int i = 0; i < index.length; i++) {
			if (index[i]){
				arrayList.add(data[i+1]);
			}
		}

		out= new Object[arrayList.size()+1][];
		out[0]=data[0];

		for (int i = 0; i < arrayList.size(); i++) {
			out[i+1]=arrayList.get(i);
		}

		return new Dataframe(out);
	}

	/**
	 * @param  label
	 * @return returns the column of the label passed in parameter
	 */
	public Dataframe loc(String label) throws Exception {
		Object[][] out=new Object[data.length][1];
		boolean labelExist=false;//true if the label exist in the Dataframe
		for (int i = 0; i < data[0].length; i++) {
			if(data[0][i].equals(label)){
				labelExist=true;
				for (int j = 0; j < data.length; j++) {
					out[j][0]=data[j][i];
				}
			}
		}
		if(!labelExist)
			throw new IllegalArgumentException("label does not exist in the dataframe");
		return new Dataframe(out);
	}

	//return true if the data[i][indice] is well typed
	private Boolean wellTyped(int indice){

		if(data[1][indice] instanceof Integer){//if there is at least one element of type int
			for (int i = 1; i <data.length ; i++) {
				if (!(data[i][indice] instanceof Integer))
					return false;
			}
			return true;
		}else if(data[1][indice] instanceof Double){//if there is at least one element of type double
			for (int i = 1; i <data.length ; i++) {
				if (!(data[i][indice] instanceof Double))
					return false;
			}
			return true;
		}else if(data[1][indice] instanceof String){//if there is at least one element of type String
			for (int i = 1; i <data.length ; i++) {
				if (!(data[i][indice] instanceof String))
					return false;
			}
			return true;
		}else{
			return false;
		}
	}


	/**
	 * @param  label
	 * @param  element
	 * @return returns the lines that are equal to the element parameter
	 */

	public Dataframe search(String label, Object element) throws Exception {
		Object[][] out;
		ArrayList<Object[]> arrayList=new ArrayList<>();
		for (int i = 0; i < data[0].length; i++) {
			if(data[0][i].equals(label)) {
				if (data[1][i] instanceof Integer && wellTyped(i) && element instanceof Integer) {
					for (int j = 1; j < data.length; j++) {//we remove the labels so j =1
						int elem = (Integer)data[j][i];
						if ((Integer)element==elem) {
							arrayList.add(data[j]);
						}
					}
				}else if(data[1][i] instanceof Double && wellTyped(i) && element instanceof Double){
					for (int j = 1; j < data.length; j++) {//we remove the labels so j =1
						double elem = (double)data[j][i];
						if ((double)element==elem) {
							arrayList.add(data[j]);
						}
					}
				}else if((data[1][i] instanceof String) && (wellTyped(i)) && (element instanceof String)) {
					for (int j = 1; j < data.length; j++) {//we remove the labels so j =1
						String elem = data[j][i].toString();
						if (element.equals(elem)) {
							arrayList.add(data[j]);
						}
					}
				}else{
					throw new IllegalArgumentException("columns and element are not compatible");
				}
			}

		}
		out=new Object[arrayList.size()+1][];
		out[0]=data[0];
		for (int i = 0; i < arrayList.size(); i++) {
			out[i+1]=arrayList.get(i);
		}
		return new Dataframe(out);
	}


	public Dataframe sum(int axis) throws Exception{
		Object[][] out;
		Object[] tmp;
		if(axis==0) {
			out = new Object[2][];
			out[0] = data[0];
			out[1] = Statistics.sum(data, axis,null);
		}
		else{
			tmp= Statistics.sum(data, axis,null);
			out=new Object[tmp.length+1][];
			out[0] = new Object[]{"sum"};
			for (int j = 0; j < tmp.length; j++) {
				out[j+1] = new Object[]{tmp[j]};
			}
		}
		return new Dataframe(out);
	}

	public Dataframe mean() throws Exception{
		Object[][] out = new Object[2][];
		out[0]=data[0];
		out[1]=Statistics.mean(data);
		return new Dataframe(out);
	}

	public Dataframe min() throws Exception{
		Object[][] out = new Object[2][];
		out[0]=data[0];
		out[1]=Statistics.min(data);
		return new Dataframe(out);
	}

	public Dataframe max() throws Exception{
		Object[][] out = new Object[2][];
		out[0]=data[0];
		out[1]=Statistics.max(data);
		return new Dataframe(out);
	}

	public Aggregate groupBy(String label, boolean sort) throws Exception{
		return new Aggregate(Statistics.groupBy(data,label,sort), label);
	}

	public Dataframe sort(String label) throws Exception{
		Object[][] out;
		Object[] tmp;
		tmp= Statistics.sort(data, label);
		out=new Object[tmp.length+1][];
		out[0] = data[0];
		for (int j = 0; j < tmp.length; j++) {
			out[j+1] = (Object[]) tmp[j];
		}
		return new Dataframe(out);
	}
}
