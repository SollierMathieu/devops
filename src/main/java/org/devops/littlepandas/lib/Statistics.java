package org.devops.littlepandas.lib;
/**
 * @author Mickael KARCHE
 */

import java.util.ArrayList;

public class Statistics {
    /**
     * @param data
     * @param axis=0 compute the sum on columns, axis=1 compute the sum on lign
     * @return return the sum based on axis param
     */
    public static Object[] sum(Object[][] data, int axis, String label) {
        Object[] out;
        int index;
        if (axis == 0)
            out = new Object[data[0].length];
        else
            out = new Object[data.length - 1];
        for (int i = 1; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                if (axis == 0) {
                    if (out[j] == null)
                        out[j] = data[i][j];
                    else {
                        if (!data[0][j].equals(label)) {
                            if (data[i][j] instanceof Integer)
                                out[j] = (Integer) out[j] + (Integer) data[i][j];
                            if (data[i][j] instanceof String)
                                out[j] = out[j] + "" + data[i][j];
                            if (data[i][j] instanceof Double)
                                out[j] = (Double) out[j] + (Double) data[i][j];
                        }
                    }
                }
                if (axis == 1) {
                    index = i - 1;
                    if (out[index] == null)
                        out[index] = 0.0d;
                    if (data[i][j] instanceof Integer)
                        out[index] = (Double) out[index] + (Integer) data[i][j];
                    if (data[i][j] instanceof Double)
                        out[index] = (Double) out[index] + (Double) data[i][j];
                }
            }
        }
        return out;
    }

    /**
     * @param data
     * @return return mean on data column
     */
    public static Object[] mean(Object[][] data) {
        Object[] out = new Object[data[0].length];
        for (int i = 1; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                if (out[j] == null)
                    out[j] = data[i][j];
                else {
                    if (data[i][j] instanceof Integer)
                        out[j] = (Integer) out[j] + (Integer) data[i][j];
                    if (data[i][j] instanceof Double)
                        out[j] = (Double) out[j] + (Double) data[i][j];
                }
            }
        }
        for (int i = 0; i < out.length; i++) {
            if (out[i] instanceof Integer) {
                out[i] = ((Integer) out[i] + 0.0);
            }
            if (out[i] instanceof Double) {
                out[i] = (Double) out[i] / (data.length - 1);
            }
            if (out[i] instanceof String) {
                out[i] = "";
            }
        }
        return out;
    }

    /**
     * @param data
     * @return return min on data column
     */
    public static Object[] min(Object[][] data) {
        Object[] out = new Object[data[0].length];
        for (int i = 1; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                if (out[j] == null) {
                    out[j] = data[i][j];
                    if (out[j] instanceof String) {
                        out[j] = "";
                    }
                } else {
                    if (data[i][j] instanceof Integer && (Integer) out[j] > (Integer) data[i][j])
                        out[j] = data[i][j];
                    if (data[i][j] instanceof Double && (Double) out[j] > (Double) data[i][j])
                        out[j] = data[i][j];
                }
            }
        }
        return out;
    }

    /**
     * @param data
     * @return return max on data column
     */
    public static Object[] max(Object[][] data) {
        Object[] out = new Object[data[0].length];
        for (int i = 1; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                if (out[j] == null)
                    out[j] = data[i][j];
                if (out[j] instanceof String) {
                    out[j] = "";
                } else {
                    if (data[i][j] instanceof Integer && (Integer) out[j] < (Integer) data[i][j])
                        out[j] = data[i][j];
                    if (data[i][j] instanceof Double && (Double) out[j] < (Double) data[i][j])
                        out[j] = data[i][j];
                }
            }
        }
        return out;
    }

    /**
     * @param list
     * @param object
     * @return true if the object is in list, false otherwise
     */
    private static int isInList(ArrayList<Object> list, Object object) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(object))
                return i;
        }
        return -1;
    }

    public static Object[][] sort(Object[][] data, String label) {
        Object[][] out;
        Object[] x;
        for (int j = 0; j < data[0].length; j++) {
            if (data[0][j].equals(label)) {
                for (int i = 1; i < data.length; i++) {
                    x = data[i];
                    int k = i;
                    if (data[i][j] instanceof Integer)
                        while (k > 1 && ((Integer) data[k - 1][j]) > (Integer) x[j]) {
                            data[k] = data[k - 1];
                            k = k - 1;
                        }
                    else if (data[i][j] instanceof Double)
                        while (k > 1 && ((Double) data[k - 1][j]) > (Double) x[j]) {
                            data[k] = data[k - 1];
                            k = k - 1;
                        }
                    else if (data[i][j] instanceof String)
                        while (k > 1 && ((String) data[k - 1][j]).compareTo((String) x[j]) > 0) {
                            data[k] = data[k - 1];
                            k = k - 1;
                        }
                    data[k] = x;
                }
            }
        }
        out=new Object[data.length-1][];
        System.arraycopy(data, 1, out, 0, out.length);
        return out;
    }

    /**
     * @param data
     * @param label
     * @return an array of data grouped by a specific label
     */
    public static Object[][][] groupBy(Object[][] data, String label, boolean sort) {
        Object[][][] out;
        ArrayList<Object> inLabel = new ArrayList<>();
        ArrayList<ArrayList<Object>> inPerLabel = new ArrayList<>();
        int index;
        ArrayList<Object> listObject;

        for (int i = 0; i < data[0].length; i++) {
            if (data[0][i].equals(label)) {
                for (int j = 1; j < data.length; j++) {
                    index = isInList(inLabel, data[j][i]);
                    if (index == -1) {
                        inLabel.add(data[j][i]);
                        listObject = new ArrayList<>();
                        listObject.add(data[j]);
                        inPerLabel.add(listObject);
                    } else {
                        listObject = inPerLabel.get(index);
                        listObject.add(data[j]);
                        inPerLabel.set(index, listObject);
                    }
                }
            }
        }

        out = new Object[inPerLabel.size()][][];

        for (int i = 0; i < inPerLabel.size(); i++) {
            out[i] = new Object[1 + inPerLabel.get(i).size()][data[0].length];
            out[i][0] = data[0];
            for (int j = 0; j < inPerLabel.get(i).size(); j++) {
                out[i][j + 1] = (Object[]) inPerLabel.get(i).get(j);
            }
        }
        if (sort)
            for (int i = 0; i < out.length; i++) {
                out[i] = Statistics.sort(out[i], label);
            }

        return out;
    }
}
