package org.devops.littlepandas.lib;
/**
 * @author Mickael KARCHE
 */

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Parsing {
    private final File fileCSV;


    /**
     *
     * @param path is the path to the CSV file
     */
    public Parsing(String path) {
        fileCSV = new File(path);
    }

    /**
     * 
     * @param value is an object that we needs to determinate the type (we convert string to real value object)
     * @return a new object with the right type and with the value in it
     */
    public Object getType(String value) {
        if (value == null) {
            return "empty";
        }
        int l;
        try {
            l = Integer.parseInt(value);
        } catch (NumberFormatException nel) {
            double d;
            try {
                d = Double.parseDouble(value);
            } catch (NumberFormatException nes) {
                return value;
            }
            return d;
        }
        return l;
    }

    /**
     *
     * @return an array of object from the CSV
     */
    public ArrayList<ArrayList<String>> getCSVasArrayList() {
        ArrayList<ArrayList<String>> out = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileCSV.getAbsoluteFile()))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.startsWith("#")) {
                	ArrayList<String> list = new ArrayList<>(Arrays.asList(line.split(",")));
                	
                	for (int i = 0; i < list.size(); i++) {
                		if ("".equals(list.get(i))) {
                			list.set(i, "na");
                		}
                	}
                	
                    out.add(list);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out;
    }

    /**
     *
     * @return convert an array obtained from the CSV to an array of multiple object with respective types of object
     */
    public Object[][] createArrayType(){
        ArrayList<ArrayList<String>> CSV=getCSVasArrayList();
        Object [][] out= new Object[CSV.size()][CSV.get(0).size()];
        for (int i = 0; i < CSV.size(); i++) {
            for (int j = 0; j < CSV.get(i).size(); j++) {
                out[i][j]=getType(CSV.get(i).get(j));
            }
        }
        return out;
    }
}
