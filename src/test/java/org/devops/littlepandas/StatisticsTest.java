package org.devops.littlepandas;

import static org.junit.Assert.*;

import org.devops.littlepandas.lib.Statistics;
import org.junit.Before;
import org.junit.Test;

public class StatisticsTest {
	Object[][] dataInt;   // [[0, 0, 0], [1, 1, 1], [2, 2, 2], [3, 3, 3], [4, 4, 4]]
	Object[][] dataDouble; // [[0.0, 0.0, 0.0], [1.5, 1.5, 1.5], [3.0, 3.0, 3.0], [4.5, 4.5, 4.5], [6.0, 6.0, 6.0]]
	Object[][] dataString; // [[0x0, 0x1, 0x2], [1x0, 1x1, 1x2], [2x0, 2x1, 2x2], [3x0, 3x1, 3x2], [4x0, 4x1, 4x2]]
	Object[][] dataMix;    // [[0, s-0, 0.0], [1, s-1, 1.5], [2, s-2, 3.0], [3, s-3, 4.5], [4, s-4, 6.0]]

	@Before
	public void setUp () {
		int sizeX = 5,
			sizeY = 3;
		dataInt = new Object[sizeX+1][sizeY];
		dataString = new Object[sizeX+1][sizeY];
		dataDouble = new Object[sizeX+1][sizeY];
		dataMix = new Object[sizeX+1][sizeY];
		
		int j;
		for (int i = 0; i < sizeX; i++) {
			for (j = 0; j < sizeY; j++) {
				dataInt[i+1][j] = i;
				dataDouble[i+1][j] = i * 1.5;
				dataString[i+1][j] = i + "x" + j;
			}
			
			dataMix[i+1][0] = i;
			dataMix[i+1][1] = "s-" + i;
			dataMix[i+1][2] = i*1.5;
		}
		for (j = 0; j < sizeY; j++) {
			dataInt[0][j] = "Col " + j;
			dataString[0][j] = "Col " + j;
			dataDouble[0][j] = "Col " + j;
			dataMix[0][j] = "Col " + j;
		}
	}


	@Test
	public void sumIntTest() {
		Integer[] expected_0 = {10, 10, 10};
		Double[] expected_1 = {0.0, 3.0, 6.0, 9.0, 12.0};
		Object[] out_axis0 = Statistics.sum(dataInt, 0,null);
		Object[] out_axis1 = Statistics.sum(dataInt, 1,null);

		assertArrayEquals(expected_0, out_axis0);
		assertArrayEquals(expected_1, out_axis1);
	}

	@Test
	public void sumDoubleTest() {
		Double[] expected_0 = {15.0, 15.0, 15.0};
		Double[] expected_1 = {0.0, 4.5, 9.0, 13.5, 18.0};
		Object[] out_axis0 = Statistics.sum(dataDouble, 0,null);
		Object[] out_axis1 = Statistics.sum(dataDouble, 1,null);

		assertArrayEquals(expected_0, out_axis0);
		assertArrayEquals(expected_1, out_axis1);
	}

	@Test
	public void sumStringTest() {
		String[] expected_0 = {"0x01x02x03x04x0", "0x11x12x13x14x1", "0x21x22x23x24x2"};
		Double[] expected_1 = {0.0, 0.0, 0.0, 0.0, 0.0};
		Object[] out_axis0 = Statistics.sum(dataString, 0,null);
		Object[] out_axis1 = Statistics.sum(dataString, 1,null);

		assertArrayEquals(expected_0, out_axis0);
		assertArrayEquals(expected_1, out_axis1);
	}


	@Test
	public void sumMixTest() {
		Object[] expected_0 = {10, "s-0s-1s-2s-3s-4", 15.0};
		Double[] expected_1 = {0.0, 2.5, 5.0, 7.5, 10.0};
		Object[] out_axis0 = Statistics.sum(dataMix, 0,null);
		Object[] out_axis1 = Statistics.sum(dataMix, 1,null);

		assertArrayEquals(expected_0, out_axis0);
		assertArrayEquals(expected_1, out_axis1);
	}

	
	@Test
	public void meanTest () {
		Double[] expectedInt = {2.0, 2.0, 2.0};
		Double[] expectedDouble = {3.0, 3.0, 3.0};
		String[] expectedString = {"","",""};
		Object[] expectedMix = {2.0, "", 3.0};
		Object[] outInt = Statistics.mean(dataInt);
		Object[] outDouble = Statistics.mean(dataDouble);
		Object[] outString = Statistics.mean(dataString);
		Object[] outMix = Statistics.mean(dataMix);

		assertArrayEquals(expectedInt, outInt);
		assertArrayEquals(expectedDouble, outDouble);
		assertArrayEquals(expectedString, outString);
		assertArrayEquals(expectedMix, outMix);
	}

	
	@Test
	public void minTest () {
		Integer[] expectedInt = {0, 0, 0};
		Double[] expectedDouble = {0.0, 0.0, 0.0};
		String[] expectedString = {"", "", ""};
		Object[] expectedMix = {0, "", 0.0};
		Object[] outInt = Statistics.min(dataInt);
		Object[] outDouble = Statistics.min(dataDouble);
		Object[] outString = Statistics.min(dataString);
		Object[] outMix = Statistics.min(dataMix);

		assertArrayEquals(expectedInt, outInt);
		assertArrayEquals(expectedDouble, outDouble);
		assertArrayEquals(expectedString, outString);
		assertArrayEquals(expectedMix, outMix);
	}
	
	@Test
	public void maxTest () {
		Integer[] expectedInt = {4, 4, 4};
		Double[] expectedDouble = {6.0, 6.0, 6.0};
		String[] expectedString = {"", "", ""};
		Object[] expectedMix = {4, "", 6.0};
		Object[] outInt = Statistics.max(dataInt);
		Object[] outDouble = Statistics.max(dataDouble);
		Object[] outString = Statistics.max(dataString);
		Object[] outMix = Statistics.max(dataMix);

		assertArrayEquals(expectedInt, outInt);
		assertArrayEquals(expectedDouble, outDouble);
		assertArrayEquals(expectedString, outString);
		assertArrayEquals(expectedMix, outMix);
	}
}
