package org.devops.littlepandas;

import static org.junit.Assert.*;

import org.devops.littlepandas.lib.Dataframe;
import org.junit.Before;
import org.junit.Test;

public class DataframeTest {
	String[][] data1;
	Object[][] dataBase;
	Object[][] dataFullString;
	Object[][] dataFullDouble;
	Object[][] dataOnlyLabel;
	Object[][] wrongType;

	@Before
	public void setUp() throws Exception {
		int sizeX = 16,
				sizeY = 5;
		data1 = new String[sizeX][sizeY];

		int j;
		for (j = 0; j < sizeY; j++) {
			data1[0][j] = "Col " + j;
		}
		for (int i = 1; i < sizeX; i++) {
			for (j = 0; j < sizeY; j++) {
				data1[i][j] = i + "x" + j;
			}
		}

		dataBase = new Object[][]{{"Prénom", "Age", "Taille"}, {"Tom", 25, 2.0}, {"Cappa", 35, 1.67}, {"Cappa", 35, 1.67}, {"Math", 35, 1.75}, {"Micka", 3, 1.80}, {"Cappa", 25, 1.67}};
		dataFullString = new Object[][]{{"Prénom", "Age", "Taille"}, {"Tom", "25", "2"}, {"Cappa", "35", "1.67"}, {"Cappa", "35", "1.67"}, {"Math", "35", "1.75"}, {"Micka", "3", "1.80"}, {"Cappa", "25", "1.67"}};
		dataFullDouble = new Object[][]{{"Prénom", "Age", "Taille"}, {"Tom", "25", 2.0}, {"Cappa", "35", 1.67}, {"Cappa", "35", 1.67}, {"Math", "35", 1.75}, {"Micka", "3", 1.80}, {"Cappa", "25", 1.67}};
		dataOnlyLabel = new Object[][]{{"Prénom", "Age", "Taille"}};
		wrongType = new Object[][]{{"Prénom", "Age", "Taille"}, {"Tom", 25, "2"}, {"Cappa", "35", "1.67"}, {"Cappa", 35, "1.67"}, {"Math", 35, "1.75"}, {"Micka", 3, "1.80"}, {"Cappa", 25, 1.67}};


	}

	@Test
	public void DataframeFromCSVTest () throws Exception {
		Dataframe df = new Dataframe("tests/test.csv");

		String expected = "  Prénom|Age|Taille|\n" + 
				"1    Tom| 25|   2.0|\n" + 
				"2  Cappa| 35|  1.67|\n" + 
				"3  Cappa| 35|  1.67|\n" + 
				"4   Math| 35|  1.75|\n" + 
				"5  Micka|  3|   1.8|\n" + 
				"6  Cappa| 25|  1.67|";

		String output = df.printAll();
		assertEquals(expected.trim(), output.trim());
	}
	
	@Test(expected=Exception.class)
	public void DataframeFromCSVWithEmptyValueTest () throws Exception {
		new Dataframe("tests/test_empty_value.csv");
	}

	@Test
	public void printOnlyLabel() throws Exception {
		Dataframe df = new Dataframe(dataOnlyLabel);
		String expected = "Prénom| Age| Taille|";

		String output = df.printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void printAllTest15x5() throws Exception {
		Dataframe df = new Dataframe(data1);

		String expected = "Col 0|Col 1|Col 2|Col 3|Col 4|\n" +
				"1    1x0|  1x1|  1x2|  1x3|  1x4|\n" +
				"2    2x0|  2x1|  2x2|  2x3|  2x4|\n" +
				"3    3x0|  3x1|  3x2|  3x3|  3x4|\n" +
				"4    4x0|  4x1|  4x2|  4x3|  4x4|\n" +
				"5    5x0|  5x1|  5x2|  5x3|  5x4|\n" +
				"6    6x0|  6x1|  6x2|  6x3|  6x4|\n" +
				"7    7x0|  7x1|  7x2|  7x3|  7x4|\n" +
				"8    8x0|  8x1|  8x2|  8x3|  8x4|\n" +
				"9    9x0|  9x1|  9x2|  9x3|  9x4|\n" +
				"10  10x0| 10x1| 10x2| 10x3| 10x4|\n" +
				"11  11x0| 11x1| 11x2| 11x3| 11x4|\n" +
				"12  12x0| 12x1| 12x2| 12x3| 12x4|\n" +
				"13  13x0| 13x1| 13x2| 13x3| 13x4|\n" +
				"14  14x0| 14x1| 14x2| 14x3| 14x4|\n" +
				"15  15x0| 15x1| 15x2| 15x3| 15x4|";

		String output = df.printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void printAllTest0x0() throws Exception {
		Dataframe df = new Dataframe(new String[0][0]);

		String expected = "";

		String output = df.printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void printAllTest1x5() throws Exception {
		String[][] data2 = new String[1][5];
		data2[0] = data1[0];

		Dataframe df = new Dataframe(data2);

		String expected = "Col 0| Col 1| Col 2| Col 3| Col 4|\n";

		String output = df.printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void printAllTest5x1() throws Exception {
		String[][] data2 = new String[5][1];
		data2[0][0] = "Col 0";
		for (int i = 1; i < 5; i++) {
			data2[i][0] = data1[i][0];
		}
		Dataframe df = new Dataframe(data2);

		String expected = "  Col 0|\n" +
				"1   1x0|\n" +
				"2   2x0|\n" +
				"3   3x0|\n" +
				"4   4x0|";

		String output = df.printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void printFirstLinesTest() throws Exception {
		Dataframe df = new Dataframe(data1);

		String expected = "  Col 0|Col 1|Col 2|Col 3|Col 4|\n" +
				"1   1x0|  1x1|  1x2|  1x3|  1x4|\n" +
				"2   2x0|  2x1|  2x2|  2x3|  2x4|\n" +
				"3   3x0|  3x1|  3x2|  3x3|  3x4|\n" +
				"4   4x0|  4x1|  4x2|  4x3|  4x4|\n" +
				"5   5x0|  5x1|  5x2|  5x3|  5x4|";

		String output = df.printFirstLines();
		assertEquals(expected.trim(), output.trim());
	}


	@Test
	public void printLastLinesTest() throws Exception {
		Dataframe df = new Dataframe(data1);

		String expected = "Col 0|Col 1|Col 2|Col 3|Col 4|\n" +
				"11  11x0| 11x1| 11x2| 11x3| 11x4|\n" +
				"12  12x0| 12x1| 12x2| 12x3| 12x4|\n" +
				"13  13x0| 13x1| 13x2| 13x3| 13x4|\n" +
				"14  14x0| 14x1| 14x2| 14x3| 14x4|\n" +
				"15  15x0| 15x1| 15x2| 15x3| 15x4|\n";

		String output = df.printLastLines();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void printLastLinesWithLessThan5RowsTest() throws Exception {
		String[][] data2 = new String[4][5];
		for (int i = 0; i < 4; i++) {
			data2[i] = data1[i];
		}
		Dataframe df = new Dataframe(data2);

		String expected = "  Col 0|Col 1|Col 2|Col 3|Col 4|\n" +
				"1   1x0|  1x1|  1x2|  1x3|  1x4|\n" +
				"2   2x0|  2x1|  2x2|  2x3|  2x4|\n" +
				"3   3x0|  3x1|  3x2|  3x3|  3x4|";

		String output = df.printLastLines();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void printFirstLinesWithLessThan5RowsTest() throws Exception {
		String[][] data2 = new String[4][5];
		for (int i = 0; i < 4; i++) {
			data2[i] = data1[i];
		}
		Dataframe df = new Dataframe(data2);

		String expected = "  Col 0|Col 1|Col 2|Col 3|Col 4|\n" +
				"1   1x0|  1x1|  1x2|  1x3|  1x4|\n" +
				"2   2x0|  2x1|  2x2|  2x3|  2x4|\n" +
				"3   3x0|  3x1|  3x2|  3x3|  3x4|";

		String output = df.printFirstLines();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void ilocIndexTest() throws Exception {
		int[] index = {1, 3, 5, 7};
		Dataframe df = new Dataframe(data1);

		String expected = "Col 0|Col 1|Col 2|Col 3|Col 4|\n" +
				"1   1x0|  1x1|  1x2|  1x3|  1x4|\n" +
				"2   3x0|  3x1|  3x2|  3x3|  3x4|\n" +
				"3   5x0|  5x1|  5x2|  5x3|  5x4|\n" +
				"4   7x0|  7x1|  7x2|  7x3|  7x4|";

		String output = df.iloc(index).printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test(expected = IllegalArgumentException.class)
	public void ilocNegativeRowIndexTestShouldThrowIllegalArgumentException() throws Exception {
		int[] index = {-5};
		Dataframe df = new Dataframe(data1);

		df.iloc(index);
	}

	@Test(expected = IllegalArgumentException.class)
	public void ilocTooLargeIndexTest() throws Exception {
		int[] index = {9999};
		Dataframe df = new Dataframe(data1);

		String expected = "";

		String output = df.iloc(index).printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void ilocBooleanColumnsTest() throws Exception {
		Boolean[] index = new Boolean[data1.length-1];
		for (int i = 0; i < data1.length-1; i++) {
			if (i % 3 == 0)
				index[i] = true;
			else
				index[i] = false;
		}

		Dataframe df = new Dataframe(data1);

		String expected = "Col 0|Col 1|Col 2|Col 3|Col 4|\n" +
				"1   1x0|  1x1|  1x2|  1x3|  1x4|\n" +
				"2   4x0|  4x1|  4x2|  4x3|  4x4|\n" +
				"3   7x0|  7x1|  7x2|  7x3|  7x4|\n" +
				"4  10x0| 10x1| 10x2| 10x3| 10x4|\n" +
				"5  13x0| 13x1| 13x2| 13x3| 13x4|";

		String output = df.iloc(index).printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test(expected = IllegalArgumentException.class)
	public void ilocBooleanColumnsTestShouldThrowIllegalArgumentException() throws Exception {
		Boolean[] index = {true, false, true};

		Dataframe df = new Dataframe(data1);
		df.iloc(index);
	}

	@Test
	public void locLabelColumnTest() throws Exception {
		Dataframe df = new Dataframe(data1);

		String expected = "Col 4|\n" +
				"1    1x4|\n" +
				"2    2x4|\n" +
				"3    3x4|\n" +
				"4    4x4|\n" +
				"5    5x4|\n" +
				"6    6x4|\n" +
				"7    7x4|\n" +
				"8    8x4|\n" +
				"9    9x4|\n" +
				"10  10x4|\n" +
				"11  11x4|\n" +
				"12  12x4|\n" +
				"13  13x4|\n" +
				"14  14x4|\n" +
				"15  15x4|";

		String output = df.loc("Col 4").printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test(expected = IllegalArgumentException.class)
	public void locLabelColumnTestShouldThrowIllegalArgumentException() throws Exception {
		Dataframe df = new Dataframe(data1);
		df.loc("Definitely Not A Column Name");
	}


	@Test
	public void searchWithRightStringElement() throws Exception {
		Dataframe df = new Dataframe(dataFullString);
		String expected = "Prénom|Age|Taille|\n" +
				"1  Micka|  3|  1.80|";

		String output = df.search("Prénom", "Micka").printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test(expected = IllegalArgumentException.class)
	public void searchWithNotStringElement() throws Exception {
		Dataframe df = new Dataframe(dataFullString);
		df.search("Prénom", 2);
	}

	@Test
	public void searchWithWrongStringElement() throws Exception {
		Dataframe df = new Dataframe(dataFullString);
		String expected = "Prénom| Age| Taille|";
		String output = df.search("Prénom", "Paul").printAll();
		assertEquals(expected.trim(), output.trim());

	}

	@Test
	public void searchWithRightIntElement() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected = "Prénom|Age|Taille|\n" +
				"1  Micka|  3|   1.8|";


		String output = df.search("Age", 3).printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test(expected = IllegalArgumentException.class)
	public void searchWithNotIntElement() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		df.search("Age", "3");
	}

	@Test
	public void searchWithWrongIntElement() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected = "Prénom| Age| Taille|";
		String output = df.search("Age", 45).printAll();
		assertEquals(expected.trim(), output.trim());

	}


	@Test
	public void searchWithRightDoubleElement() throws Exception {
		Dataframe df = new Dataframe(dataFullDouble);
		String expected = "Prénom|Age|Taille|\n" +
				"1  Micka|  3|   1.8|";


		String output = df.search("Taille", 1.80).printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test(expected = IllegalArgumentException.class)
	public void searchWithNotDoubleElement() throws Exception {
		Dataframe df = new Dataframe(dataFullDouble);
		df.search("Taille", "1.80");
	}

	@Test
	public void searchWithWrongDoubleElement() throws Exception {
		Dataframe df = new Dataframe(dataFullDouble);
		String expected = "Prénom| Age| Taille|";
		String output = df.search("Taille", 1.30).printAll();
		assertEquals(expected.trim(), output.trim());

	}


	@Test
	public void testMin() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected = "Prénom|Age|Taille|" + "\n" + "1       |  3|  1.67|";
		String output = df.min().printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void testMax() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected = "Prénom|Age|Taille|" + "\n" + "1       | 35|   2.0|\n";
		String output = df.max().printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void testMean() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected = "Prénom|               Age|Taille|\n"  + "1       |26.333333333333332|  1.76|\n";
		String output = df.mean().printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void testSumLine() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected = "                       Prénom|Age|Taille|\n" +
				"1 TomCappaCappaMathMickaCappa|158| 10.56|";
		String output = df.sum(0).printAll();
		assertEquals(expected.trim(),output.trim());
	}
	@Test
	public void testSumColumn() throws Exception {
		Object[][]dataBaseTest = new Object[][]{{"Prénom", "Age", "Taille"}, {"Tom", 25, 2.0}, {"Cappa", 35, 1.67}, {"Cappa", 35, 1.67}, {"Math", 35, 1.75}};
		Dataframe df = new Dataframe(dataBaseTest);
		String expected = "    sum|\n" +
				"1  27.0|\n" +
				"2 36.67|\n" +
				"3 36.67|\n" +
				"4 36.75|";
		String output= df.sum(1).printAll();
		assertEquals(expected.trim(),output.trim());
	}
	
	@Test
	public void testGroupBy() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected =
				"Prénom|Age|Taille|\n"+
				"1    Tom| 25|   2.0|\n"+"\n"+
				"  Prénom|Age|Taille|\n"+
				"1  Cappa| 35|  1.67|\n"+
				"2  Cappa| 35|  1.67|\n"+
				"3  Cappa| 25|  1.67|\n"+"\n"+
				"  Prénom|Age|Taille|\n"+
				"1   Math| 35|  1.75|\n"+"\n"+
				"  Prénom|Age|Taille|\n"+
				"1  Micka|  3|   1.8|\n";
		String output = df.groupBy("Prénom",false).printAll();
		assertEquals(expected.trim(),output.trim());
	}

	@Test
	public void testGroupBySort() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected ="Tom| 25| 2.0| \n" + 
				"  Cappa|35|1.67|\n" + 
				"1 Cappa|35|1.67|\n" + 
				"2 Cappa|25|1.67|\n" + 
				"\n" + 
				"Math| 35| 1.75| \n" + 
				"Micka| 3| 1.8|";
		String output = df.groupBy("Prénom",true).printAll();
		assertEquals(expected.trim(),output.trim());
	}
	
	@Test
	public void testSortInt() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected = "  Prénom|Age|Taille|\n" +
				"1  Micka|  3|   1.8|\n" +
				"2    Tom| 25|   2.0|\n" +
				"3  Cappa| 25|  1.67|\n" +
				"4  Cappa| 35|  1.67|\n" +
				"5  Cappa| 35|  1.67|\n" +
				"6   Math| 35|  1.75|\n";
		String output = df.sort("Age").printAll();
		assertEquals(expected.trim(),output.trim());
	}
	@Test
	public void testSortString() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected = "  Prénom|Age|Taille|\n" +
				"1  Cappa| 35|  1.67|\n" +
				"2  Cappa| 35|  1.67|\n" +
				"3  Cappa| 25|  1.67|\n" +
				"4   Math| 35|  1.75|\n" +
				"5  Micka|  3|   1.8|\n" +
				"6    Tom| 25|   2.0|";
		String output = df.sort("Prénom").printAll();
		assertEquals(expected.trim(),output.trim());
	}

	@Test
	public void testSortDouble() throws Exception {
		Dataframe df = new Dataframe(dataBase);
		String expected = "  Prénom|Age|Taille|\n" +
				"1  Cappa| 35|  1.67|\n" +
				"2  Cappa| 35|  1.67|\n" +
				"3  Cappa| 25|  1.67|\n" +
				"4   Math| 35|  1.75|\n" +
				"5  Micka|  3|   1.8|\n" +
				"6    Tom| 25|   2.0|";
		String output = df.sort("Taille").printAll();
		assertEquals(expected.trim(),output.trim());
	}
	
}
