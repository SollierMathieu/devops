package org.devops.littlepandas;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import org.devops.littlepandas.lib.Parsing;
import org.junit.Before;
import org.junit.Test;

public class ParsingTest {
	Parsing p;

	@Before
	public void setUp () {
		p = new Parsing("tests/test.csv");
	}

	@Test
	public void getTypeOfIntShouldReturnInt () {
		Integer n1 = -10,
			n2 = 0,
			n3 = 10;

		assertEquals("java.lang.Integer", p.getType("" + n1).getClass().getName());
		assertEquals(n1, p.getType("" + n1));
		assertEquals("java.lang.Integer", p.getType("" + n2).getClass().getName());
		assertEquals(n2, p.getType("" + n2));
		assertEquals("java.lang.Integer", p.getType("" + n3).getClass().getName());
		assertEquals(n3, p.getType("" + n3));
	}
	
	@Test
	public void getTypeOfDoubleShouldReturnDouble () {
		double n1 = -10.5,
			n2 = 0,
			n3 = 10.125;

		assertEquals("java.lang.Double", p.getType("" + n1).getClass().getName());
		assertEquals(n1, p.getType("" + n1));
		assertEquals("java.lang.Double", p.getType("" + n2).getClass().getName());
		assertEquals(n2, p.getType("" + n2));
		assertEquals("java.lang.Double", p.getType("" + n3).getClass().getName());
		assertEquals(n3, p.getType("" + n3));
	}
	
	@Test
	public void getTypeOfStringShouldReturnString () {
		String s1 = "a string",
			s2 = "1.5.6",
			s3 = "--0";

		assertEquals("java.lang.String", p.getType(s1).getClass().getName());
		assertEquals(s1, p.getType(s1));
		assertEquals("java.lang.String", p.getType(s2).getClass().getName());
		assertEquals(s2, p.getType("" + s2));
		assertEquals("java.lang.String", p.getType(s3).getClass().getName());
		assertEquals(s3, p.getType("" + s3));
	}
	
	@Test
	public void getTypeOfNullShouldReturnNull () {
		assertEquals("java.lang.String", p.getType(null).getClass().getName());
		assertEquals("empty", p.getType(null));
	}
	
	@Test
	public void getCSVasArrayListTest () {
		ArrayList<ArrayList<String>> out = p.getCSVasArrayList();

		String[][] expected = {{"Prénom", "Age", "Taille"},
				{"Tom", "25", "2.0"},
				{"Cappa", "35", "1.67"},
				{"Cappa", "35", "1.67"},
				{"Math", "35", "1.75"},
				{"Micka", "3", "1.80"},
				{"Cappa", "25", "1.67"}};

		for (int i = 0; i < expected.length; i++) {
			for (int j = 0; j < expected[0].length;j++) {
				assertEquals(expected[i][j], out.get(i).get(j));
			}
		}
	}
	
	@Test
	public void getCSVasArrayListShouldEmptyList () {
		Parsing p = new Parsing("");

		// Redirect error output stream in order to keep the console output clean
		ByteArrayOutputStream void_err = new ByteArrayOutputStream();
		System.setErr(new PrintStream(void_err));
		
		ArrayList<ArrayList<String>> out = p.getCSVasArrayList();
		
		assertEquals(0, out.size());
	}
	
	@Test
	public void createArrayTypeTest () {
        Object[][] out = p.createArrayType();

        Object[][] expected = {{"Prénom", "Age", "Taille"},
				{"Tom", 25, 2.0},
				{"Cappa", 35, 1.67},
				{"Cappa", 35, 1.67},
				{"Math", 35, 1.75},
				{"Micka", 3, 1.80},
				{"Cappa", 25, 1.67}};
        
        assertArrayEquals(expected, out);
	}
}
