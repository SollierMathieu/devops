package org.devops.littlepandas;

import static org.junit.Assert.*;

import org.devops.littlepandas.lib.Aggregate;
import org.devops.littlepandas.lib.Dataframe;
import org.junit.Before;
import org.junit.Test;

public class AggregateTest {
	String[][] data1;
	Object[][] dataBase;
	Object[][] dataFullString;
	Object[][] dataFullDouble;
	Object[][] dataOnlyLabel;
	Object[][] wrongType;

	@Before
	public void setUp() {
		int sizeX = 16,
				sizeY = 5;
		data1 = new String[sizeX][sizeY];

		int j;
		for (j = 0; j < sizeY; j++) {
			data1[0][j] = "Col " + j;
		}
		for (int i = 1; i < sizeX; i++) {
			for (j = 0; j < sizeY; j++) {
				data1[i][j] = i + "x" + j;
			}
		}

		dataBase = new Object[][]{{"Prénom", "Age", "Taille"}, {"Tom", 25, 2.0}, {"Cappa", 35, 1.67}, {"Cappa", 35, 1.67}, {"Math", 35, 1.75}, {"Micka", 3, 1.80}, {"Cappa", 25, 1.67}};
		dataFullString = new Object[][]{{"Prénom", "Age", "Taille"}, {"Tom", "25", "2"}, {"Cappa", "35", "1.67"}, {"Cappa", "35", "1.67"}, {"Math", "35", "1.75"}, {"Micka", "3", "1.80"}, {"Cappa", "25", "1.67"}};
		dataFullDouble = new Object[][]{{"Prénom", "Age", "Taille"}, {"Tom", "25", 2.0}, {"Cappa", "35", 1.67}, {"Cappa", "35", 1.67}, {"Math", "35", 1.75}, {"Micka", "3", 1.80}, {"Cappa", "25", 1.67}};
		dataOnlyLabel = new Object[][]{{"Prénom", "Age", "Taille"}};
		wrongType = new Object[][]{{"Prénom", "Age", "Taille"}, {"Tom", 25, "2"}, {"Cappa", "35", "1.67"}, {"Cappa", 35, "1.67"}, {"Math", 35, "1.75"}, {"Micka", 3, "1.80"}, {"Cappa", 25, 1.67}};


	}

	@Test
	public void sumAxis1Test () throws Exception {
		Aggregate agg = new Dataframe(dataBase).groupBy("Prénom",false);
		String expected = "    sum|\n" + 
				"1  27.0|\n" + 
				"2 36.67|\n" + 
				"3 36.67|\n" + 
				"4 26.67|\n" + 
				"5 36.75|\n" + 
				"6   4.8|";

		String output = agg.sum(1).combine().printAll();
		assertEquals(expected.trim(), output.trim());
	}
	
	@Test
	public void sumAxis0Test () throws Exception {
		Aggregate agg = new Dataframe(dataBase).groupBy("Prénom",false);
		String expected = "  Prénom|Age|Taille|\n" + 
				"1    Tom| 25|   2.0|\n" + 
				"2  Cappa| 95|  5.01|\n" + 
				"3   Math| 35|  1.75|\n" + 
				"4  Micka|  3|   1.8|";

		String output = agg.sum(0).combine().printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void meanTest () throws Exception {
		Aggregate agg = new Dataframe(dataBase).groupBy("Prénom",false);
		String expected = "  Prénom|               Age|Taille|\n" + 
				"1    Tom|              25.0|   2.0|\n" + 
				"2  Cappa|31.666666666666668|  1.67|\n" + 
				"3   Math|              35.0|  1.75|\n" + 
				"4  Micka|               3.0|   1.8|";

		String output = agg.mean().combine().printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void minTest () throws Exception {
		Aggregate agg = new Dataframe(dataBase).groupBy("Prénom",false);
		String expected = "  Prénom|Age|Taille|\n" + 
				"1    Tom| 25|   2.0|\n" + 
				"2  Cappa| 25|  1.67|\n" + 
				"3   Math| 35|  1.75|\n" + 
				"4  Micka|  3|   1.8|";

		String output = agg.min().combine().printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void maxTest () throws Exception {
		Aggregate agg = new Dataframe(dataBase).groupBy("Prénom",false);
		String expected = "  Prénom|Age|Taille|\n" + 
				"1    Tom| 25|   2.0|\n" + 
				"2  Cappa| 35|  1.67|\n" + 
				"3   Math| 35|  1.75|\n" + 
				"4  Micka|  3|   1.8|";

		String output = agg.max().combine().printAll();
		assertEquals(expected.trim(), output.trim());
	}

	@Test
	public void getTest () throws Exception {
		Aggregate agg = new Dataframe(dataBase).groupBy("Prénom",false);
		String expected = "  Prénom|Age|Taille|\n" + 
				"1  Cappa| 35|  1.67|\n" + 
				"2  Cappa| 35|  1.67|\n" + 
				"3  Cappa| 25|  1.67|";

		String output = agg.get(1).printAll();
		assertEquals(expected.trim(), output.trim());
	}
	
}
