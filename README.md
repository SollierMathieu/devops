# Littlepandas

[![pipeline status](https://gitlab.com/SollierMathieu/devops/badges/master/pipeline.svg)](https://gitlab.com/SollierMathieu/devops/) [![coverage report](https://gitlab.com/SollierMathieu/devops/badges/master/coverage.svg)](https://gitlab.com/SollierMathieu/devops/) [![team name](https://img.shields.io/badge/Powered%20by-Team%20Verveine-blue.svg)](https://gitlab.com/SollierMathieu/devops/)

Subset of Pandas Python library implemented in Java.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
* [Maven](https://maven.apache.org/)
* Java 11

### Installing
Clone with SSH

```bash
git clone git@gitlab.com:SollierMathieu/devops.git
```

Clone with HTTPS

```bash
git clone https://gitlab.com/SollierMathieu/devops.git
```

For building the project sources into an executable JAR run :

```bash
mvn package

# Output JAR file location :
ls ./target/littlepandas.jar
```

## Running the tests
Runs unit tests using [JUnit4](https://junit.org/junit4/) :

```bash
mvn test
```

## Running a demo from DockerHub
Run a demo pulling an image from DockerHub
```bash
docker pull littlepandas/demo:latest
docker run littlepandas/demo:latest
```

## Deployment on DockerHub
The littlepandas.jar executable is available in an Ubuntu image on [DockerHub](https://hub.docker.com/r/littlepandas/littlepandas) :

```bash
docker pull littlepandas/littlepandas:latest
```

## Tools used
* [Maven](https://maven.apache.org/) - Dependency Management and Build Automation
* [JUnit4](https://junit.org/junit4/) - Unit Testing
* [Docker](https://www.docker.com/) - Deployment
* [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) - Continuous Integration, Continuous Delivery and Continuous Deployment

## Features
### Dataframe

* Can be initialized with CSV file or a 2D Object Array
* Handles `Integer`, `Double` and `String`
* Display methods to print in the console
* Select rows or columns by number or label
* Search methods : groupBy, search
* Statistic methods on different axis : min, max, mean, sum
* Sort method

## Feedback
* [Maven](https://maven.apache.org/)

Setting up Maven was easy because we had already worked with it before, but adding and managing plugins was harder as it's not so intuitive.
Nevertheless, Maven remains useful and practical for dependencies management and the knowledge we gained will be useful for our future projects.

* [JUnit4](https://junit.org/junit4/)

Unit testing is very important, and we were glad to finally be able to do some in one of our project. We realized how useful tests are when it comes to changing the implementation or ensuring that our implementation covers all the cases. However, we are beginners with unit testing in general and specifically with JUnit, so we only did simple tests, not going deep into the possibilities.

* [Docker](https://www.docker.com/)

We have had some troubles to identify in what way Docker could be used and be useful in our project. We eventually decided to deploy an image on Dockerhub everytime we release a version of our library. We also release a demo as a docker image aside the library image.

* [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)

We were impatient to work with DevOps concepts, and to create a project with a CI/CD approach. That hasn't been so easy, as we couldn't test the configuration file offline, and it was a bit complicated to integrate code coverage check and dockerhub deployement, but we learned a lot and doing CI/CD will be much more easier for our next projects.

* [Pandas](https://pandas.pydata.org/)

Choosing Pandas as the  library to (partially) implement was a good choice, because it was not hard so it allowed us to fully focus on the DevOps approach of the project.

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
